This test is designed to assess your familiarity with the .Net language, as well as basic problem solving skills. There are two parts to the test.


For part one, the you are to create a variety of filter methods that operate on queryables of type Person. All filter methods should be extension methods that return an altered queryable, so that they may be chained together, similar to existing LINQ operations.

The following filters should be created as specified:

FilterByGender - Should take an (existing) enum of type Gender, and return a Queryable of Person matching the specified gender.

FilterByState - Should take an (existing) enum of type State, and return a Queryable of Person matching the specified state.

FilterByAge - Should take an integer, and return a Queryable of Person who match the specified age. 

FilterByZodiac - Should take a (new) Enum of type ZodiacSign, and return a Queryable of Person who have the matching (western astrological) zodiac sign. This should return the correct results when called on the existing data.



For part two, you are to create a Car respository (CarManager) that manages a private IDbContext. A new CarManager should construct a FakeDatabase to serve in place a dependency injection. 

You should create a car DTO with three properties, Year, Make, and Model. Your car manager should implement an ICarManager that has the following methods:

GetCars - Should return all cars in the database

GetCarsByMake - Should return all cars from the database with the specified make

GetCarsByModel - Should return all cars from the database with the specified model

