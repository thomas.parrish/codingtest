﻿using System;

namespace Data.Entities
{
    public class PersonEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string State { get; set; }
    }
}
