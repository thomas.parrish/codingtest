﻿namespace Data.Entities
{
    public class CarEntity
    {
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
    }
}
