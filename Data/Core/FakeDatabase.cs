﻿using System.Collections.Generic;
using System.Linq;


namespace Data.Core
{
    using Entities;
    using Newtonsoft.Json;

    public class FakeDatabase : IDbContext
    {
        public IQueryable<PersonEntity> People { get; set; }
        public IQueryable<CarEntity> Cars { get; set; }
        
        public FakeDatabase()
        {
            People = JsonConvert.DeserializeObject<HashSet<PersonEntity>>(Resources.Data.People).AsQueryable();
            Cars = JsonConvert.DeserializeObject<HashSet<CarEntity>>(Resources.Data.Cars).AsQueryable();
        }
    }
}
