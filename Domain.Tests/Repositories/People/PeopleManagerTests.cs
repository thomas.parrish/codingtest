﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Domain.Tests.Repositories.People
{
    using Data.Core;
    using Data.Entities;
    using Domain.Repositories.People;
    using Moq;
    using Shouldly;

    [TestClass]
    public class PeopleManagerTests
    {
        private Mock<IDbContext> _mockDb;

        [TestInitialize]
        public void Init()
        {
            _mockDb = new Mock<IDbContext>();

            var results = new List<PersonEntity>()
            {
                new PersonEntity
                {
                    FirstName = "BOB",
                    LastName = "ROSS",
                    Gender = Gender.Male.ToString(),
                    DateOfBirth = new DateTime(1942, 10, 29),
                    State = State.FL.ToString()
                }
            }.AsQueryable();


            _mockDb.Setup(x => x.People).Returns(results);
        }

        [TestMethod]
        public async Task DefaultConstructorShouldPopulateFakeDatabaseFromResource()
        {
            var mgr = new PeopleManager();

            var result = await mgr.GetPeople();
            result.Count().ShouldBe(300);
        }

        [TestMethod]
        public async Task DIConstructorShouldReturnExpectedResults()
        {
            var mgr = new PeopleManager(_mockDb.Object);

            var result = await mgr.GetPeople();
            result.Count().ShouldBe(1);
        }
    }
}
