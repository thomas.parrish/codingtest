﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Domain.Tests
{
    using DTO;
    using Domain.Extensions;
    using Shouldly;

    [TestClass]
    public class FilterByNameTests
    {
        private IQueryable<Person> _people;

        public FilterByNameTests()
        {
            _people = new List<Person>()
            {
                new Person
                {
                    FirstName = "BOB",
                    LastName = "ROSS",
                    Gender = Gender.Male,
                    DateOfBirth = new DateTime(1942, 10, 29),
                    State = State.FL
                },
                new Person
                {
                    FirstName = "DIANA",
                    LastName = "ROSS",
                    Gender = Gender.Male,
                    DateOfBirth = new DateTime(1944, 3, 26),
                    State = State.CA
                },
                new Person
                {
                    FirstName = "JOHN",
                    LastName = "BOBBITT",
                    Gender = Gender.Male,
                    DateOfBirth = new DateTime(1967, 3, 23),
                    State = State.NY
                },
            }.AsQueryable();
        }

        [TestMethod]
        public void ItShouldFilterCorrectlyIfNameIsFound()
        {
            _people.FilterByLastName("ROSS").Count().ShouldBe(2);
        }

        [TestMethod]
        public void ItShouldFilterCaseInsensitive()
        {
            _people.FilterByLastName("ROss").Count().ShouldBe(2);
        }

        [TestMethod]
        public void ItShouldReturnEmptyQueryableIfNoResultIsFound()
        {
            _people.FilterByLastName("nope").Count().ShouldBe(0);
        }
    }
}
